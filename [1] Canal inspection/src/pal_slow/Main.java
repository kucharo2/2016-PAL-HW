package pal_slow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

/**
 * @Author Roman KuchĂˇr <kucharrom@gmail.com>.
 */
public class Main {

    private static long startTime;

    private static List<Integer>[] adjencyList;
    private static List<Integer>[] componentAdjencyList;

    // list of edge "object"
    private static int[][] edgeParams;
    private static int FROM = 0;
    private static int TO = 1;
    private static int INSPECT = 2;


    // list of node "object"
    private static int[][] nodeParams;
    private static final int INDEX = 0;
    private static final int LOWLINK = 1;
    private static final int PRED = 2;
    private static final int INSTACK = 3;
    private static final int IS_ROOT = 4;
    private static final int COMPONENT = 5;

    // list of component "object"
    private static int[][] componentParams;
    private static int CANALS_TO_INSPECT = 0;
    private static int ROOT_COMPONENT = 1;
    private static int RESULT = 2;

    private static int index;
    private static int stack;
    private static int sccNumber;
    private static int nodesCount;
    private static int edgesCount;

    public static void main(String[] args) throws IOException {
        readData();
        startFindingScc();
        System.out.println(findBestPath());
        long endTime = System.nanoTime();
        System.out.println(endTime - startTime);
    }

    private static int findBestPath() {
        int max = Integer.MIN_VALUE;

        for (int component = 0; component < sccNumber + 1; component++) {
            if (componentParams[component][ROOT_COMPONENT] == 0) {
                max = Math.max(max, componentParams[component][CANALS_TO_INSPECT]);
            } else {
                max = Math.max(max, dfs(component));
            }
        }
        return max;
    }

    private static int dfs(int component) {
        int max = 0;
        for (Integer nodeEdge : componentAdjencyList[component]) {
            int componentTo = nodeParams[edgeParams[nodeEdge][TO]][COMPONENT];
            int possible = edgeParams[nodeEdge][INSPECT];
            if (componentParams[componentTo][RESULT] == -1) {
                componentParams[componentTo][RESULT] = dfs(componentTo);
            }
            possible += componentParams[componentTo][RESULT];
            max = Math.max(max, possible);
        }
        int counted = componentParams[component][CANALS_TO_INSPECT];
        return counted + max;
    }

    private static void startFindingScc() {
        index = 0;
        stack = -1;
        sccNumber = -1;
        for (int node = 0; node < nodesCount; node++) {
            if (nodeParams[node][INDEX] == 0) {
                findScc(node);
            }
        }

        componentAdjencyList = new LinkedList[sccNumber + 1];
        for (int i = 0; i < sccNumber + 1; i++) {
            componentAdjencyList[i] = new LinkedList<>();
        }

        for (int edge = 0; edge < edgesCount; edge++) {
            int componentFrom = nodeParams[edgeParams[edge][FROM]][COMPONENT];
            int componentTo = nodeParams[edgeParams[edge][TO]][COMPONENT];
            if (componentFrom == componentTo) {
                if (edgeParams[edge][INSPECT] == 1) {
                    componentParams[componentFrom][CANALS_TO_INSPECT]++;
                }
            } else {
                componentAdjencyList[componentFrom].add(edge);
                componentParams[componentTo][ROOT_COMPONENT] = 0;
            }
        }

        for (int i = 0; i < sccNumber + 1; i++) {
            System.out.println("Component " + i + " with " + componentParams[i][CANALS_TO_INSPECT] + " canals to inspect");
            int edgeIdsSum = 0;
            StringBuilder stringBuilder = new StringBuilder();
            if (componentAdjencyList[i] != null) {
                stringBuilder.append(componentAdjencyList[i].size());
            }
            stringBuilder.append(" edges with sum = " + edgeIdsSum);
            System.out.println(stringBuilder);
        }
    }

    private static void findScc(int node) {
        nodeParams[node][INDEX] = nodeParams[node][LOWLINK] = ++index;
        push(node);
        adjencyList[node].stream().forEach(edge -> {
            int nextNode = edgeParams[edge][TO];
            if (nodeParams[nextNode][INDEX] == 0) {
                // not yet visited
                findScc(nextNode);
                if (nodeParams[node][LOWLINK] > nodeParams[nextNode][LOWLINK]) {
                    nodeParams[node][LOWLINK] = nodeParams[nextNode][LOWLINK];
                }
            } else if (nodeParams[nextNode][INSTACK] == 1) {
                if (nodeParams[node][LOWLINK] > nodeParams[nextNode][LOWLINK]) {
                    nodeParams[node][LOWLINK] = nodeParams[nextNode][LOWLINK];
                }
            }
        });

        if (nodeParams[node][LOWLINK] == nodeParams[node][INDEX]) {
            ++sccNumber;
            int popNode;
            do {
                popNode = pop(stack);
                nodeParams[popNode][COMPONENT] = sccNumber;
            } while (node != popNode);
        }
    }

    private static void push(int node) {
        nodeParams[node][PRED] = stack;
        nodeParams[node][INSTACK] = 1;
        stack = node;
    }

    private static int pop(int nodeNumber) {
        stack = nodeParams[nodeNumber][PRED];
        nodeParams[nodeNumber][PRED] = -1;
        nodeParams[nodeNumber][INSTACK] = 0;
        return nodeNumber;
    }

    private static void readData() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        nodesCount = readInt(br);
        startTime = System.nanoTime();
        adjencyList = new LinkedList[nodesCount];
        nodeParams = new int[nodesCount][6];
        componentParams = new int[nodesCount][3];
        for (int i = 0; i < nodesCount; i++) {
            adjencyList[i] = new LinkedList<>();
            // no edge assigned, so every node is a root
            nodeParams[i][IS_ROOT] = 1;
            nodeParams[i][COMPONENT] = -1;
            componentParams[i][ROOT_COMPONENT] = 1;
            componentParams[i][RESULT] = -1;
        }

        edgesCount = readInt(br);
        edgeParams = new int[edgesCount][4];
        for (int edge = 0; edge < edgesCount; edge++) {
            int from = readInt(br);
            int to = readInt(br);
            int needToInspect = readInt(br);
            adjencyList[from].add(edge);
            edgeParams[edge][FROM] = from;
            edgeParams[edge][TO] = to;
            edgeParams[edge][INSPECT] = needToInspect;
            nodeParams[to][IS_ROOT] = 0;
        }
    }

    public static int readInt(BufferedReader in) throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }


}