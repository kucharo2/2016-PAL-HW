package pal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * @Author Roman Kuchár <kucharrom@gmail.com>.
 */
public class Main {

    private static long startTime;

    private static List<Integer>[] adjencyList;
    private static List<Integer>[] componentAdjencyList;
    private static List<Integer> unresolvedEdges = new ArrayList<>();
    private static int[] canalsToInspectByComponentIndex;

    private static int[] edgeFrom;
    private static int[] edgeTo;
    private static int[] edgeInspect;


    private static int[] nodeIndex;
    private static int[] nodeLowlink;
    private static int[] nodePred;
    private static int[] nodeInstack;
    private static int[] nodeComponent;

    private static int[] componentValues;
    private static int[] componentIsRoot;
    private static int[] componentResult;

    private static int index;
    private static int stack;
    private static int sccNumber;
    private static int nodesCount;

    public static void main(String[] args) throws IOException {
        readData();
        startFindingScc();
        System.out.println(findBestPath());
//        long endTime = System.nanoTime();
//        System.out.println(endTime - startTime);
    }

    private static int findBestPath() {
        int max = Integer.MIN_VALUE;
        for (int component = 0; component < sccNumber + 1; component++) {
            if (componentIsRoot[component] == 0) {
                max = Math.max(max, componentValues[component]);
            } else {
                max = Math.max(max, dfs(component));
            }
        }
        return max;
    }

    private static int dfs(int component) {
        int max = 0;
        if (componentAdjencyList[component] != null) {
            for (Integer nodeEdge : componentAdjencyList[component]) {
                int componentTo = nodeComponent[edgeTo[nodeEdge]];
                int possible = edgeInspect[nodeEdge];
                if (componentResult[componentTo] == -1) {
                    componentResult[componentTo] = dfs(componentTo);
                }
                possible += componentResult[componentTo];
                max = Math.max(max, possible);
            }
        }
        int counted = componentValues[component];
        return counted + max;
    }

    private static void startFindingScc() {
        index = 0;
        stack = -1;
        sccNumber = -1;
        for (int node = 0; node < nodesCount; node++) {
            if (nodeIndex[node] == 0) {
                findScc(node);
            }
        }

        componentAdjencyList = new ArrayList[sccNumber + 1];
        unresolvedEdges.stream().forEach(edge -> {
            int componentFrom = nodeComponent[edgeFrom[edge]];
            int componentTo = nodeComponent[edgeTo[edge]];
            if (componentAdjencyList[componentFrom] == null) {
                componentAdjencyList[componentFrom] = new ArrayList<Integer>();
            }
            componentAdjencyList[componentFrom].add(edge);
            componentIsRoot[componentTo] = 0;

        });

    }

    private static void findScc(int node) {
        nodeIndex[node] = nodeLowlink[node] = ++index;
        push(node);
        adjencyList[node].stream().forEach(edge -> {
            int nextNode = edgeTo[edge];
            if (nodeIndex[nextNode] == 0) {
                // not yet visited
                findScc(nextNode);
                updateLowlink(node, nextNode, edge);
            } else if (nodeInstack[nextNode] == 1) {
                updateLowlink(node, nextNode, edge);
            } else {
                unresolvedEdges.add(edge);
            }
        });

        if (nodeLowlink[node] == nodeIndex[node]) {
            ++sccNumber;
            componentValues[sccNumber] = canalsToInspectByComponentIndex[nodeLowlink[node]];
            int popNode;
            do {
                popNode = pop(stack);
                nodeComponent[popNode] = sccNumber;
            } while (node != popNode);
        }
    }

    private static void updateLowlink(int node, int nextNode, int edge) {
        int nextNodeLowlink = nodeLowlink[nextNode];
        int thisNodeLowlink = nodeLowlink[node];
        if (thisNodeLowlink >= nextNodeLowlink) {
            // this edge is part of the component
            int actual = 0;
            if (thisNodeLowlink != nextNodeLowlink) {
                actual = canalsToInspectByComponentIndex[thisNodeLowlink];
                canalsToInspectByComponentIndex[thisNodeLowlink] = 0;
            }
            canalsToInspectByComponentIndex[nextNodeLowlink] += edgeInspect[edge] + actual;

            nodeLowlink[node] = nextNodeLowlink;

        } else {
            if (nodeComponent[node] > -1 || nodeComponent[nextNode] > -1) {
                // one of the nodes has been added to some component.
                unresolvedEdges.add(edge);
            } else {
                int actual = canalsToInspectByComponentIndex[nextNodeLowlink];
                canalsToInspectByComponentIndex[nextNodeLowlink] = 0;
                canalsToInspectByComponentIndex[thisNodeLowlink] += edgeInspect[edge] + actual;
            }
        }
    }

    private static void push(int node) {
        nodePred[node] = stack;
        nodeInstack[node] = 1;
        stack = node;
    }

    private static int pop(int node) {
        stack = nodePred[node];
        nodePred[node] = -1;
        nodeInstack[node] = 0;
        return node;
    }

    private static void readData() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        nodesCount = readInt(br);
        startTime = System.nanoTime();
        adjencyList = new ArrayList[nodesCount];
        nodeIndex = new int[nodesCount];
        nodeLowlink = new int[nodesCount];
        nodePred = new int[nodesCount];
        nodeInstack = new int[nodesCount];
        nodeComponent = new int[nodesCount];
        componentValues = new int[nodesCount];
        componentIsRoot = new int[nodesCount];
        componentResult = new int[nodesCount];
        for (int i = 0; i < nodesCount; i++) {
            adjencyList[i] = new ArrayList<>();
            nodeComponent[i] = -1;
            componentIsRoot[i] = 1;
            componentResult[i] = -1;
        }

        int edgesCount = readInt(br);
        edgeFrom = new int[edgesCount];
        edgeTo = new int[edgesCount];
        edgeInspect = new int[edgesCount];
        canalsToInspectByComponentIndex = new int[edgesCount];
        Arrays.fill(canalsToInspectByComponentIndex, 0);
        for (int edge = 0; edge < edgesCount; edge++) {
            int from = readInt(br);
            int to = readInt(br);
            int needToInspect = readInt(br);
            adjencyList[from].add(edge);
            edgeFrom[edge] = from;
            edgeTo[edge] = to;
            edgeInspect[edge] = needToInspect;
        }
    }

    public static int readInt(BufferedReader in) throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }

}
