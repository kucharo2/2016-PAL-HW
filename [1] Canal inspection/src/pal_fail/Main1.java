package pal_fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * @Author Roman KuchĂˇr <kucharrom@gmail.com>.
 */
public class Main1 {

    private static List<Integer>[] adjencyList;
    private static List<Integer>[] componentAdjencyList;
    //    private static Set<Integer> edges;
    private static List<Integer> unresolvedEdges = new LinkedList<>();
    private static int[] canalsToInspectByComponentIndex;

    // list of edge "object"
    private static int[][] edgeParams;
    private static int FROM = 0;
    private static int TO = 1;
    private static int INSPECT = 2;


    // list of node "object"
    private static int[][] nodeParams;
    private static final int INDEX = 0;
    private static final int LOWLINK = 1;
    private static final int PRED = 2;
    private static final int INSTACK = 3;
    private static final int IS_ROOT = 4;
    private static final int COMPONENT = 5;

    // list of component "object"
    private static int[][] componentParams;
    private static int CANALS_TO_INSPECT = 0;
    private static int ROOT_COMPONENT = 1;
    private static int RESULT = 2;

    private static int index;
    private static int stack;
    private static int sccNumber;
    private static int nodesCount;

    public static void main(String[] args) throws IOException {
        readData();
        startFindingScc();
        System.out.println(findBestPath());
//        System.out.println("520");
    }

    private static int findBestPath() {
        int max = Integer.MIN_VALUE;
        for (int component = 0; component < sccNumber + 1; component++) {
            if (componentParams[component][ROOT_COMPONENT] == 0) {
                max = Math.max(max, componentParams[component][CANALS_TO_INSPECT]);
            } else {
                max = Math.max(max, dfs(component));
            }
        }
        return max;
    }

    private static int dfs(int component) {
//        System.out.println("run dfs for " + component);
        int max = 0;
        if (componentAdjencyList[component] != null) {
            for (Integer nodeEdge : componentAdjencyList[component]) {
                int componentTo = nodeParams[edgeParams[nodeEdge][TO]][COMPONENT];
                int possible = edgeParams[nodeEdge][INSPECT];
                if (componentParams[componentTo][RESULT] == -1) {
                    componentParams[componentTo][RESULT] = dfs(componentTo);
                }
                possible += componentParams[componentTo][RESULT];
                max = Math.max(max, possible);
            }
        }
        int counted = componentParams[component][CANALS_TO_INSPECT];
        return counted + max;
    }

    private static void startFindingScc() {
        index = 0;
        stack = -1;
        sccNumber = -1;
        for (int node = 0; node < nodesCount; node++) {
            if (nodeParams[node][INDEX] == 0) {
                findScc(node);
            }
        }

        componentAdjencyList = new LinkedList[sccNumber + 1];
        unresolvedEdges.stream().forEach(edge -> {
            int componentFrom = nodeParams[edgeParams[edge][FROM]][COMPONENT];
            int componentTo = nodeParams[edgeParams[edge][TO]][COMPONENT];
            if (componentAdjencyList[componentFrom] == null) {
                componentAdjencyList[componentFrom] = new LinkedList<Integer>();
            }
            componentAdjencyList[componentFrom].add(edge);
            componentParams[componentTo][ROOT_COMPONENT] = 0;

        });

//        edges.stream().forEach(edge -> {
//            int componentFrom = nodeParams[edgeParams[edge][FROM]][COMPONENT];
//            int componentTo = nodeParams[edgeParams[edge][TO]][COMPONENT];
//            if (componentFrom == componentTo) {
//                if (edgeParams[edge][INSPECT] == 1) {
//                    componentParams[componentFrom][CANALS_TO_INSPECT]++;
//                }
//            } else {
//                if (componentAdjencyList[componentFrom] == null) {
//                    componentAdjencyList[componentFrom] = new LinkedList<>();
//                }
//                componentAdjencyList[componentFrom].add(edge);
//                componentParams[componentTo][ROOT_COMPONENT] = 0;
//            }
//        });
    }

    private static void findScc(int node) {
        nodeParams[node][INDEX] = nodeParams[node][LOWLINK] = ++index;
        push(node);
        adjencyList[node].stream().forEach(edge -> {
//            edges.remove(edge);
            int nextNode = edgeParams[edge][TO];
            if (nodeParams[nextNode][INDEX] == 0) {
                // not yet visited
                findScc(nextNode);
                if (nodeParams[node][LOWLINK] >= nodeParams[nextNode][LOWLINK]) {
                    // this edge is part of the component
                    int actual = 0;
                    if (nodeParams[node][LOWLINK] != nodeParams[nextNode][LOWLINK]) {
                        actual = canalsToInspectByComponentIndex[nodeParams[node][LOWLINK]];
                        canalsToInspectByComponentIndex[nodeParams[node][LOWLINK]] = 0;
                    }
//                    if (actual > 0) {
//                        System.out.println("adding " + actual + " to component " + nodeParams[nextNode][LOWLINK] + "[edge = " + edge + "]");
//                    }
                    canalsToInspectByComponentIndex[nodeParams[nextNode][LOWLINK]] += edgeParams[edge][INSPECT] + actual;

                    nodeParams[node][LOWLINK] = nodeParams[nextNode][LOWLINK];

                } else {
                    if (nodeParams[node][COMPONENT] > -1 || nodeParams[nextNode][COMPONENT] > -1) {
                        // one of the nodes has been added to some component.
                        unresolvedEdges.add(edge);
                    } else {
                        canalsToInspectByComponentIndex[nodeParams[node][LOWLINK]] += edgeParams[edge][INSPECT];
                    }
                }
            } else if (nodeParams[nextNode][INSTACK] == 1) {
                if (nodeParams[node][LOWLINK] >= nodeParams[nextNode][LOWLINK]) {
                    // this edge is part of the component
                    int actual = 0;
                    if (nodeParams[node][LOWLINK] != nodeParams[nextNode][LOWLINK]) {
                        actual = canalsToInspectByComponentIndex[nodeParams[node][LOWLINK]];
                        canalsToInspectByComponentIndex[nodeParams[node][LOWLINK]] = 0;
                    }
                    canalsToInspectByComponentIndex[nodeParams[nextNode][LOWLINK]] += edgeParams[edge][INSPECT] + actual;

                    nodeParams[node][LOWLINK] = nodeParams[nextNode][LOWLINK];

                } else {
                    if (nodeParams[node][COMPONENT] > -1 || nodeParams[nextNode][COMPONENT] > -1) {
                        // one of the nodes has been added to some component.
                        unresolvedEdges.add(edge);
                    } else {
                        canalsToInspectByComponentIndex[nodeParams[node][LOWLINK]] += edgeParams[edge][INSPECT];
                    }
                }
            } else {
                unresolvedEdges.add(edge);
            }
        });

        if (nodeParams[node][LOWLINK] == nodeParams[node][INDEX]) {
            ++sccNumber;
//            System.out.println("component with index " + nodeParams[node][LOWLINK]);
            componentParams[sccNumber][CANALS_TO_INSPECT] = canalsToInspectByComponentIndex[nodeParams[node][LOWLINK]];
            int popNode;
            do {
                popNode = pop(stack);
                nodeParams[popNode][COMPONENT] = sccNumber;
            } while (node != popNode);
        }
    }

    private static void push(int node) {
        nodeParams[node][PRED] = stack;
        nodeParams[node][INSTACK] = 1;
        stack = node;
    }

    private static int pop(int nodeNumber) {
        stack = nodeParams[nodeNumber][PRED];
        nodeParams[nodeNumber][PRED] = -1;
        nodeParams[nodeNumber][INSTACK] = 0;
        return nodeNumber;
    }

    private static void readData() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        nodesCount = readInt(br);
        adjencyList = new LinkedList[nodesCount];
        nodeParams = new int[nodesCount][6];
        componentParams = new int[nodesCount][3];
        for (int i = 0; i < nodesCount; i++) {
            adjencyList[i] = new LinkedList<>();
            // no edge assigned, so every node is a root
            nodeParams[i][IS_ROOT] = 1;
            nodeParams[i][COMPONENT] = -1;
            componentParams[i][ROOT_COMPONENT] = 1;
            componentParams[i][RESULT] = -1;
        }

        int edgesCount = readInt(br);
        edgeParams = new int[edgesCount][4];
//        edges = new HashSet<>(edgesCount);
        canalsToInspectByComponentIndex = new int[edgesCount];
        Arrays.fill(canalsToInspectByComponentIndex, 0);
        for (int edge = 0; edge < edgesCount; edge++) {
//            edges.add(edge);
            int from = readInt(br);
            int to = readInt(br);
            int needToInspect = readInt(br);
            adjencyList[from].add(edge);
            edgeParams[edge][FROM] = from;
            edgeParams[edge][TO] = to;
            edgeParams[edge][INSPECT] = needToInspect;
            nodeParams[to][IS_ROOT] = 0;
        }
    }

    public static int readInt(BufferedReader in) throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }


}