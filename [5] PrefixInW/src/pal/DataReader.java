package pal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author Roman Kuchár <kucharrom@gmail.com>.
 */
class DataReader {

    private State[] states;
    private String prefix;

    private long startTime;

    DataReader() throws IOException {
        readData();
    }

    private void readData() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int numberOfStates = readInt(br);
        startTime = System.nanoTime();

        states = new State[numberOfStates];

        for (int i = 0; i < numberOfStates; i++) {
            states[i] = new State(i);
        }

        int languageSize = readInt(br);
        for (int i = 0; i < numberOfStates; i++) {
            readState(br, languageSize);
        }
        prefix = br.readLine();
    }

    private void readState(BufferedReader in, int languageSize) throws IOException {
        int stateNumber = readInt(in);
        State state = states[stateNumber];
        state.isFinal = readFinalState(in);
        List<State> transitionsForLetter = new ArrayList<>();

        int letterInAscii;
        int number = 0;
        boolean dig = false;
        for (int c = 0; (c = in.read()) != -1; ) {
            if (c == 10) {
                // new line
                if(number > 0) {
                    transitionsForLetter.add(states[number]);
                }
                break;
            } else if (c >= 'a' && c < 'a' + languageSize) {
                // new letter
                transitionsForLetter = new ArrayList<>();
                letterInAscii = c;
                state.transitions.put(letterInAscii, transitionsForLetter);
                number = 0;
            } else if (c >= '0' && c <= '9') {
                // reading number
                dig = true;
                number = number * 10 + c - '0';
            } else if (dig) {
                // add state number to transition list
                transitionsForLetter.add(states[number]);
                dig = false;
                number = 0;
            }
        }
    }

    private int readInt(BufferedReader in) throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }

    private boolean readFinalState(BufferedReader in) throws IOException {
        for (int c = 0; (c = in.read()) != -1; ) {
            if (c == 'F') {
                return true;
            } else if (c == '-') {
                return false;
            }
        }
        throw new RuntimeException("Failed to load if state is final attribute.");
    }

    public State[] getStates() {
        return states;
    }

    public String getPrefix() {
        return prefix;
    }

    public long getStartTime() {
        return startTime;
    }
}
