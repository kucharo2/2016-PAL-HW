package pal;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Author Roman Kuchár <kucharrom@gmail.com>.
 */
public class Main {

    private static int minLength = Integer.MAX_VALUE;
    private static int maxLength;

    private static Set<Integer> statesAfterPrefix = new HashSet<>();
    private static String prefix;


    public static void main(String[] args) throws IOException {
        DataReader dataReader = new DataReader();

        State[] states = dataReader.getStates();

        prefix = dataReader.getPrefix();
        maxLength = prefix.length();
        findStatesAfterPrefix(states[0], 0);

//        System.out.println((System.nanoTime() - dataReader.getStartTime()) / 1000 / 1000);

//        // now i have states in which i can be after prefix
//        // now i need to find paths to final state
        statesAfterPrefix.stream().forEach(stateNumber -> {
            State state = states[stateNumber];
            findAcceptedWord(state, prefix.length());
        });



        System.out.println(minLength + " " + maxLength);
    }

    private static void findStatesAfterPrefix(State state, int depth) {
        state.transitions.get((int) prefix.charAt(depth)).stream().forEach(nextState -> {
            if (depth + 1 == prefix.length()) {
                statesAfterPrefix.add(nextState.number);
            } else {
                findStatesAfterPrefix(nextState, depth + 1);
            }
        });
    }

    private static void findAcceptedWord(State state, int actualLength) {
        if (state.maxLengthToFinal >= 0 || state.minLengthToFinal >= 0) {
            saveMinAndMax(actualLength + state.maxLengthToFinal);
            saveMinAndMax(actualLength + state.minLengthToFinal);
            return;
        }
        if (state.isFinal) {
            saveMinAndMax(actualLength);
            state.minLengthToFinal = 0;
            state.maxLengthToFinal = 0;
        }
        state.transitions.values().stream().forEach(characterTransitions -> {
            characterTransitions.stream().forEach(nextState -> {
                findAcceptedWord(nextState, actualLength + 1);
                state.updateMinMaxLengthToFinal(nextState);
            });
        });
    }

    private static void saveMinAndMax(int actual) {
        if (actual < minLength) {
            minLength = actual;
        }
        if (actual > maxLength) {
            maxLength = actual;
        }
    }

}
