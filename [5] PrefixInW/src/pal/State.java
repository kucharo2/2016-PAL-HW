package pal;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author Roman Kuchár <kucharrom@gmail.com>.
 */
public class State {

    int number;
    boolean isFinal = false;
    int minLengthToFinal = -1;
    int maxLengthToFinal = -1;
    Map<Integer, List<State>> transitions = new HashMap<>();

    public State(int number) {
        this.number = number;
    }

    public void updateMinMaxLengthToFinal(State nextState) {
        if (nextState.minLengthToFinal >= 0) {
            if(minLengthToFinal < 0) {
                minLengthToFinal = nextState.minLengthToFinal + 1;
            } else if(nextState.minLengthToFinal + 1 < minLengthToFinal) {
                minLengthToFinal = nextState.minLengthToFinal + 1;
            }
        }
        if (nextState.maxLengthToFinal >= 0) {
            if(maxLengthToFinal < 0) {
                maxLengthToFinal = nextState.maxLengthToFinal + 1;
            } else if (nextState.maxLengthToFinal + 1 > maxLengthToFinal) {
                maxLengthToFinal = nextState.maxLengthToFinal + 1;
            }
        }
    }

}
