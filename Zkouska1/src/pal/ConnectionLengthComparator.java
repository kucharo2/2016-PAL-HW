package pal;

import java.util.Comparator;

/**
 * @Author Roman Kuchár <kucharrom@gmail.com>.
 */
public class ConnectionLengthComparator implements Comparator<Connection> {

    private int magicLength;

    public ConnectionLengthComparator(int magicLength) {
        this.magicLength = magicLength;
    }

    @Override
    public int compare(Connection o1, Connection o2) {
        int o1Length = o1.getLength() == magicLength ? 0 : o1.getLength();
        int o2Length = o2.getLength() == magicLength ? 0 : o2.getLength();

        if (o1Length > o2Length) {
            return 1;
        } else if (o1Length < o2Length) {
            return -1;
        } else {
            return 0;
        }
    }
}
