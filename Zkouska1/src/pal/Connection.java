package pal;

/**
 * @Author Roman Kuchár <kucharrom@gmail.com>.
 */
class Connection {

    private int from;
    private int to;
    private int length;

    public Connection(int from, int to, int length) {
        this.from = from;
        this.to = to;
        this.length = length;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}