package pal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * @Author Roman Kuchár <kucharrom@gmail.com>.
 */
public class Main {

    private static List<Connection> connections;
    private static Map<Integer, Integer> sameLengthCount = new HashMap<>();

    private static int detectorsCount;
    private static int maxLengthCount = 0;

    // min length of result spanning tree
    private static Long minResultLength = Long.MAX_VALUE;


    public static void main(String[] args) throws IOException {
        readData();

        Map<Integer, Integer> sameLengthCountSorted = new LinkedHashMap<>();


        //sort by value, and reserve, 10,9,8,7,6...
        sameLengthCount.entrySet().stream()
                .sorted(Map.Entry.<Integer, Integer>comparingByValue().reversed())
                .forEachOrdered(x -> sameLengthCountSorted.put(x.getKey(), x.getValue()));

        for (Integer length : sameLengthCountSorted.keySet()) {
            if (sameLengthCountSorted.get(length) != maxLengthCount) {
                break;
            }
            kruskalAlgorithm(connections, detectorsCount, length);
        }
        System.out.println(minResultLength);
    }

    public static void kruskalAlgorithm(List<Connection> edges, int nodeCount, int magicLength) {
        UnionFind unionFind = new UnionFind(nodeCount);
        long minLength = 0;
//        int minMagicLength = 0;
        int spanningTreeSize = 0;
        edges.sort(new ConnectionLengthComparator(magicLength));
        int i = 0;
        while (i != edges.size() && spanningTreeSize != nodeCount - 1) {
            Connection connection = edges.get(i);
            if (unionFind.find(connection.getFrom()) != unionFind.find(connection.getTo())) {
                spanningTreeSize++;
                unionFind.union(connection.getFrom(), connection.getTo());

                minLength += connection.getLength();
//                minMagicLength += connection.getLength() == magicLength ? 0 : connection.getLength();
            }
            i++;
        }
//        System.out.println(magicLength + ": " + minMagicLength + " " + minLength);
        if (minResultLength > minLength) {
            minResultLength = minLength;
        }
    }

    private static void readData() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        detectorsCount = readInt(br);
        int connectionsCount = readInt(br);

        connections = new ArrayList<>(connectionsCount);
        for (int i = 0; i < connectionsCount; i++) {
            // data have node number from one. Need to decrement so it will be from 0.
            int from = readInt(br) - 1;
            int to = readInt(br) - 1;
            int length = readInt(br);
            connections.add(new Connection(from, to, length));
            Integer integer = sameLengthCount.get(length);
            if (integer == null) {
                sameLengthCount.put(length, 1);
            } else {
                int newLengthCount = integer + 1;
                sameLengthCount.put(length, newLengthCount);
                if (maxLengthCount < newLengthCount) {
                    maxLengthCount = newLengthCount;
                }
            }
        }
    }

    private static int readInt(BufferedReader in) throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }

}
