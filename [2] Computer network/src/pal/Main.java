package pal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * @Author Roman Kuchár <kucharrom@gmail.com>.
 */
public class Main {

    private static List<Integer>[] adjencyList;
    private static List<String>[] certificatesList;
    private static int[] nodesDegree;

    private static int nodesCount;

    public static void main(String[] args) throws IOException {
        readData();
        computeMastersAndCertificates();
        countIsomorphicNetworks();
    }

    private static void countIsomorphicNetworks() {
        String firstKindCertificate = null;
        String secondKindCertificate = null;
        int firstKingCount = 0;
        int secondKingCount = 0;
        for (int i = 0; i < nodesCount; i++) {
            if(nodesDegree[i] > 1) {
                String masterCertificate = computeCertificate(i);
                if (firstKindCertificate == null || firstKindCertificate.equals(masterCertificate)) {
                    firstKindCertificate = masterCertificate;
                    firstKingCount++;
                } else if (secondKindCertificate == null || secondKindCertificate.equals(masterCertificate)) {
                    secondKindCertificate = masterCertificate;
                    secondKingCount++;
                }
            }
        }

        if (firstKingCount < secondKingCount) {
            System.out.println(firstKingCount + " " + secondKingCount);
        } else {
            System.out.println(secondKingCount + " " + firstKingCount);
        }
    }

    private static void computeMastersAndCertificates() {
        for (int node = 0; node < nodesCount; node++) {
            fillCertificateList(node, -1);
        }
    }

    private static void fillCertificateList(int node, int previousNode) {
        if (nodesDegree[node] != 1) {
            return;
        }
        nodesDegree[node]--;
        (adjencyList[node]).stream().filter(nextNode -> nextNode != previousNode).forEach(nextNode -> {
            nodesDegree[nextNode]--;
            certificatesList[nextNode].add(computeCertificate(node));
            fillCertificateList(nextNode, node);
        });
    }

    private static String computeCertificate(int node) {
        if (certificatesList[node].size() < 1) {
            return "01";
        } else {
            Object[] certificates = certificatesList[node].toArray();
            Arrays.sort(certificates);
            StringBuilder sb = new StringBuilder();
            sb.append("0");
            for (Object certificate : certificates) {
                sb.append((String) certificate);
            }
            sb.append("1");
            return sb.toString();
        }
    }

    private static void readData() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        nodesCount = readInt(br);
        adjencyList = new ArrayList[nodesCount];
        certificatesList = new ArrayList[nodesCount];
        nodesDegree = new int[nodesCount];
        for (int i = 0; i < nodesCount; i++) {
            adjencyList[i] = new ArrayList<>();
            certificatesList[i] = new ArrayList<>();
        }

        int edgesCount = readInt(br);
        for (int edge = 0; edge < edgesCount; edge++) {
            int from = readInt(br);
            int to = readInt(br);
            adjencyList[from].add(to);
            adjencyList[to].add(from);
            nodesDegree[from]++;
            nodesDegree[to]++;
        }
    }

    public static int readInt(BufferedReader in) throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }

}
