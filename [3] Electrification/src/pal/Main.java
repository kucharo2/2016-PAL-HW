package pal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @Author Roman Kuchár <kucharrom@gmail.com>.
 */
public class Main {

    private static int villagesCount;
    private static int riverConnectionsCount;
    private static int bestPrice = Integer.MAX_VALUE;

    private static List<Integer> connections;
    private static List<Integer> riverConnections;

    // connection params
    private static int[] fromConnection;
    private static int[] toConnection;
    private static Integer[] priceConnection;

    public static void main(String[] args) throws IOException {
        readData();
        Collections.sort(connections, new ConnectionPriceComparator());
        findBestPrice();
        System.out.println(bestPrice);
    }

    private static void findBestPrice() {
        List<List<Integer>> combinations = findCombinations(riverConnections, riverConnectionsCount);

        for (List<Integer> combination : combinations) {
            UnionFind unionFind = new UnionFind(villagesCount);
            int actualPrice = kruskalAlgorithm(combination, unionFind);
            if(actualPrice < bestPrice) {
                bestPrice = actualPrice;
            }
        }
    }


    public static int kruskalAlgorithm(List<Integer> combination, UnionFind unionFind) {
        int bridgesPrice = 0;
        int start;
        int end;

        for (Integer connection: combination) {
            bridgesPrice = bridgesPrice + priceConnection[connection];

            if(bridgesPrice >= bestPrice){
                break;
            }

            start = unionFind.find(fromConnection[connection]);
            end = unionFind.find(toConnection[connection]);
            unionFind.union(start, end);
        }

        for (Integer connection : connections) {
            start = unionFind.find(fromConnection[connection]);
            end = unionFind.find(toConnection[connection]);

            if(start != end) {
                unionFind.union(start, end);
                bridgesPrice = bridgesPrice + priceConnection[connection];

                if(bridgesPrice >= bestPrice){
                    break;
                }
            }
        }

        return bridgesPrice;

    }

    private static List<List<Integer>> findCombinations(List<Integer> values, int size) {

        if (0 == size) {
            return Collections.singletonList(Collections.emptyList());
        }

        if (values.isEmpty()) {
            return Collections.emptyList();
        }

        List<List<Integer>> combination = new ArrayList<>();

        Integer actual = values.iterator().next();

        List<Integer> subSet = new ArrayList<>(values);
        subSet.remove(actual);

        List<List<Integer>> subSetCombination = findCombinations(subSet, size - 1);

        for (List<Integer> set : subSetCombination) {
            List<Integer> newSet = new ArrayList<>(set);
            newSet.add(0, actual);
            combination.add(newSet);
        }

        combination.addAll(findCombinations(subSet, size));

        return combination;
    }

    private static void readData() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int connectionsCount = readInt(br);
        villagesCount = readInt(br);
        int numberOfVillagesOnLeftBank = readInt(br);
        riverConnectionsCount = readInt(br);

        connections = new ArrayList<>(connectionsCount);
        riverConnections = new ArrayList<>(riverConnectionsCount);

        fromConnection = new int[connectionsCount];
        toConnection = new int[connectionsCount];
        priceConnection = new Integer[connectionsCount];

        for (int i = 0; i < connectionsCount; i++) {
            int from = readInt(br);
            fromConnection[i] = from;
            int to = readInt(br);
            toConnection[i] = to;
            priceConnection[i] = readInt(br);

            if (from <= numberOfVillagesOnLeftBank && to > numberOfVillagesOnLeftBank ||
                    from > numberOfVillagesOnLeftBank && to <= numberOfVillagesOnLeftBank) {
                riverConnections.add(i);
            } else {
                connections.add(i);
            }
        }
    }

    private static int readInt(BufferedReader in) throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }

    private static class UnionFind {

        public int[] parent;
        public int[] rank;

        public UnionFind(int max) {

            parent = new int[max];
            rank = new int[max];

            for (int i = 0; i < max; i++) {
                parent[i] = i;
            }
        }

        public int find(int i) {

            int p = parent[i];
            if (i == p) {
                return i;
            }
            return parent[i] = find(p);
        }


        public void union(int i, int j) {

            int root1 = find(i);
            int root2 = find(j);

            if (root2 == root1) return;

            if (rank[root1] > rank[root2]) {
                parent[root2] = root1;
            } else if (rank[root2] > rank[root1]) {
                parent[root1] = root2;
            } else {
                parent[root2] = root1;
                rank[root1]++;
            }
        }

    }

    private static class ConnectionPriceComparator implements Comparator<Integer> {
        @Override
        public int compare(Integer o1, Integer o2) {
            return priceConnection[o1].compareTo(priceConnection[o2]);
        }
    }

}
