package ondro;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.nio.channels.Pipe;
import java.util.*;

public class Main {

    private static int VILLAGES_COUNT;
    private static int LINES_COUNT;
    private static int LEFT_BANK_COUNT;
    private static int LINES_OVER_RIVER_COUNT;

    private static int END_OF_FILE = -1;

    private static ArrayList<Edge> edges;
    private static ArrayList<Edge> edgesOverRiver;

    private static int totalPrice = Integer.MAX_VALUE;

    public static void main(String[] args) throws Exception {

//        long startRead = System.currentTimeMillis();

//        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("datapub/pub09.in")));

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        LINES_COUNT = readNextInteger(bufferedReader);
        VILLAGES_COUNT = readNextInteger(bufferedReader);
        LEFT_BANK_COUNT = readNextInteger(bufferedReader);
        LINES_OVER_RIVER_COUNT = readNextInteger(bufferedReader);

        int startVillage, endVillage, linePrice;
        edges = new ArrayList<>(LINES_COUNT);
        edgesOverRiver = new ArrayList<>();

        for (int i = 0; i < LINES_COUNT; i++) {

            startVillage = readNextInteger(bufferedReader);
            endVillage = readNextInteger(bufferedReader);
            linePrice = readNextInteger(bufferedReader);

            Edge newEdge = new Edge(startVillage, endVillage, linePrice);

            if (startVillage <= LEFT_BANK_COUNT && endVillage > LEFT_BANK_COUNT || endVillage <= LEFT_BANK_COUNT && startVillage > LEFT_BANK_COUNT) {

                edgesOverRiver.add(newEdge);

            }else{

                edges.add(newEdge);

            }

        }

//        long endRead = System.currentTimeMillis();

        /*System.out.println(edges.toString());
        System.out.println(edgesOverRiver.toString());*/

//        long startComb = System.currentTimeMillis();

        System.out.println(edgesOverRiver.size());
        System.out.println(LINES_OVER_RIVER_COUNT);
        List<List<Edge>> combinations = createCombinations(edgesOverRiver, LINES_OVER_RIVER_COUNT);
//        System.out.println("Finded over river: " + edgesOverRiver.size());
//        System.out.println("Over river max lines: " + LINES_OVER_RIVER_COUNT);
//        System.out.println("Combinations count: " + combinations.size());

//        long endComb = System.currentTimeMillis();
//        long startKruskal = System.currentTimeMillis();

        Collections.sort(edges);

        UnionFindSet unionFindSet = new UnionFindSet(VILLAGES_COUNT);

        for (List<Edge> combination : combinations) {
            //System.out.println(findCombinations.toString());
            int actualPrice = priceCounter(edges, combination, unionFindSet);
            System.out.println("Actual price " + actualPrice);
            if(actualPrice < totalPrice) {
                System.out.println("changed price from " + totalPrice + " to " + actualPrice);
                totalPrice = actualPrice;

            }

            for (int i = 0; i < VILLAGES_COUNT; i++) {

                unionFindSet.nodes[i].id = i;
                unionFindSet.nodes[i].parent = null;

            }

        }

//        long endKruskal = System.currentTimeMillis();
//
//        System.out.println("ReadTime: " + (endRead-startRead));
//        System.out.println("CombinationsTime: " + (endComb-startComb));
//        System.out.println("KruskalTime: " + (endKruskal-startKruskal));

        //System.out.printf("Total price: ");
        System.out.println(totalPrice);

    }

    public static int priceCounter(List<Edge> edges, List<Edge> combination, UnionFindSet unionFindSet) {

        int price = 0;
        int cycles = 0;
        int size = 0;

        int start;
        int end;

        for (Edge e: combination) {

            if(price >= totalPrice){

                break;

            }

            start = unionFindSet.find(e.getStart());
            System.out.println("start1 " + start);
            end = unionFindSet.find(e.getEnd());
            System.out.println("end1 " + end);

            if(start == end) {

                cycles++;

            }

            size++;
            unionFindSet.union(start, end);
            price = price + e.getPrice();

        }

        int index = 0;
        int whileCycles =  VILLAGES_COUNT - 1;
        if(cycles > 0) {

            whileCycles =  whileCycles + cycles;

        }

        Edge e;

        while (index != edges.size() && size != whileCycles) {

            if(price >= totalPrice){

                break;

            }

            e = edges.get(index);

            start = unionFindSet.find(e.getStart());
            System.out.println("start2 " + start);
            end = unionFindSet.find(e.getEnd());
            System.out.println("end2 " + end);

            if(start != end) {

                size++;
                unionFindSet.union(start, end);
                price = price + e.getPrice();

            }

            index++;

        }

        if(size == whileCycles){
            return price;
        }

        return Integer.MAX_VALUE;

    }

    static class UnionFindSet {

        private Node[] nodes;

        public UnionFindSet(int nodeCount) {

            nodes = new Node[nodeCount];

            for (int i = 0; i < nodeCount; i++) {

                nodes[i] = new Node();
                nodes[i].id = i;

            }

        }

        public int union(int nodeIndexStart, int nodeIndexEnd) {

            Node kompStart = nodes[nodeIndexStart];
            Node kompEnd = nodes[nodeIndexEnd];

            kompEnd.parent = kompStart;

            return kompStart.id;

        }

        public int find(int nodeIndex) {

            int high = 0;
            Node currentNode = nodes[nodeIndex];

            while (currentNode.parent != null) {
                if(high > nodes.length ){
                    break;
                }
                currentNode = currentNode.parent;
                high++;

            }

            if (high > 1) {

                repair(nodeIndex, currentNode.id);

            }

            return currentNode.id;

        }

        private void repair(int nodeIndex, int rootId) {

            Node currentNode = nodes[nodeIndex];

            while (currentNode.id != rootId) {

                Node swap = currentNode.parent;
                currentNode.parent = nodes[rootId];
                currentNode = swap;

            }

        }

        private static class Node {

            Node parent;

            int id;

        }

    }

    public static int readNextInteger(BufferedReader bufferedReader) throws Exception {

        boolean isDigit = false;
        int number = 0;

        for (int i = 0; (i = bufferedReader.read()) != END_OF_FILE; ) {

            if (i >= '0' && i <= '9') {

                isDigit = true;
                number =  number * 10 + i - '0';

            } else {

                if (isDigit) break;

            }

        }

        return  number;

    }

    public static List<List<Edge>> createCombinations(ArrayList<Edge> elements, int size) {

        List<List<Edge>> combinations = new ArrayList<List<Edge>>();

        if (size == 0) {

            combinations.add(new ArrayList<Edge>());

        } else if (size <= elements.size()) {

            ArrayList<Edge> remainingElements = new ArrayList<Edge>(elements);
            Edge lastElement = remainingElements.remove(remainingElements.size()-1);

            List<List<Edge>> combinationsExclusive = createCombinations(remainingElements, size);
            List<List<Edge>> combinationsInclusive = createCombinations(remainingElements, size-1);

            for (List<Edge> combination : combinationsInclusive) {

                combination.add(lastElement);

            }

            combinations.addAll(combinationsExclusive);
            combinations.addAll(combinationsInclusive);

        }

        return combinations;

    }

    static class Edge implements Comparable<Edge>{

        private final int start;
        private final int end;
        private final int price;

        public Edge(int start, int end, int price) {
            this.start = start;
            this.end = end;
            this.price = price;
        }

        public int compareTo(Edge o) {
            if (this.getPrice() > o.getPrice()) {
                return 1;
            } else if (this.getPrice() < o.getPrice()) {
                return -1;
            } else {
                return 0;
            }
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }

        public int getPrice() {
            return price;
        }

        @Override
        public String toString() {
            return "Edge{" +
                    "start=" + start +
                    ", end=" + end +
                    ", price=" + price +
                    '}';
        }
    }

}
